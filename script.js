const testimonials = [
  {
    name : "Cherise G",
    photoUrl:
    "https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?w=500&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTJ8fHVzZXJzfGVufDB8fDB8fHww",

    text :"apple's mobile devices are the epitome of innovation and style. Their sleek designs and cutting-edge technology make them a must-have for tech-savvy individuals. I can't imagine my life without my apple phone, it's a game-changer!"
  },
  { 
    name : "Johnson",
    photoUrl : 
  "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?q=80&w=1374&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D",

  text : "I am extremely impressed with the mobile products from Apple. The sleek design and top-notch performance exceeded all my expectations. I highly recommend Apple to anyone looking for quality and innovation."
  },
  {

    name : "Robert",
    photoUrl : 
  "https://images.unsplash.com/photo-1633332755192-727a05c4013d?w=800&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8Mnx8dXNlcnN8ZW58MHx8MHx8fDA%3D",

  text : "I can't imagine my life without the amazing mobile products from apple. Their innovation truly sets them apart from the competition. The sleek design and cutting-edge technology make every day more enjoyable."

  },

];
 const imgEl = document.querySelector("img");
 const textEl = document.querySelector(".text");
 const usernameEl = document.querySelector(".username");

 let idx = 0;
 updateTestimonial();

 function updateTestimonial() {
  const { name, photoUrl, text } = testimonials[idx];
  imgEl.src = photoUrl;
  textEl.innerText = text;
  usernameEl.innerText = name;
  idx++;
  if (idx === testimonials.length) {
    idx = 0;
  }
  setTimeout(() => {
    updateTestimonial();
  }, 3000);
}
